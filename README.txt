INTRODUCTION
------------

This module is an extension for Metatag module for multi domain site installation that is based on Domain Access module.
It gives users the ability to configure meta tags separately for each page and each domain.

REQUIREMENTS
------------
This module requires the following modules:

 * Metatag
 * Page manager (Ctools)
 * Domain Access

INSTALLATION
------------

 Install as you would normally install a contributed Drupal module. See:
 https://drupal.org/documentation/install/modules-themes/modules-7
 for further information.

CONFIGURATION
-------------

 * Go to admin/config/search/metatags
 * Choose the page from the list that you want to configure
 * Add necessary meta tags
 * Save your updates

MAINTAINERS
-----------

Current maintainers:
 * Andriy Zahura (shkiper) - https://www.drupal.org/u/shkiper
